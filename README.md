# suncalc

plots sunrise/noon/sunset for given position and given date
done with python3-astral


## run for example (see code)

python3 suncalc/suncalc-plot.py \
    --prnminmax=1 \
    --prnplot=1 \
    --datefrom=20211101 \
    --datetill=20221031 \
    --exportfile=testdata/suncalc.txt \
    --exportform=txt \
    --place=sadri \
    --region=rajasthan \
    --tz=Asia/Kolkata \
    --lat=25.18773\
    --lon=73.45125\
    --elev=360\
    --stepd=5\
    --plotwidth=40
