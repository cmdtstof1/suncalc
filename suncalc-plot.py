#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

'''
schnell wurf für sun time calculation ascii plot

plots sunrise/noon/sunset for given position and given date
done with python3-astral


example:
sadri lat=25.18773 lon=73.45125
01.11.2021 - 28.02.2022
date        daylong *sunrise - *noon - sunset*
          1          2        3         4         5         6         7
0123456789*123456789*123456789*123456789*123456789*123456789*123456789*
01.11.2021 11:12:04 *06:43............*12:19..........17:55*
06.11.2021 11:05:49 *06:46............*12:19..........17:52*
11.11.2021 10:59:55 *06:50............*12:20..........17:50*
16.11.2021 10:54:27 *06:53............*12:20..........17:48*
21.11.2....


TODO: 
- rise und set graph stimmt nicht!!!! > done
- noon zentrieren 12*15 > done
- add shortest/longest day, earliest/latest rise/down > done
- 

'''

from datetime import *
from astral import *

import sys

class Suncalcplot:
    
    def __init__(self, *opt, **karg):
        self.larg = larg
        self.errcode = 0
        self.opt = self.setDefaultOpt() #1: default options
        self.parseArg(self.larg) #2: parse arg and overwrite options <list >dict
        self.cfg = self.loadcfg("cfg.json")
        
    def setDefaultOpt(self):
        opt = {
            '--prnplot': 1, # print plot
            '--prnminmax': 1, #print min/max infos
            '--datefrom': '20211101', 
            '--datetill': '20220228',
            '--place': 'sadri', # #will be shown on plot
            '--region': 'rajasthan', #
            '--tz': 'Asia/Kolkata', #tzinfo
            '--lat': 25.18773,
            '--lon': 73.45125,
            '--elev': 360,
            '--stepd': 5, #day stepper
            '--exportfile': 'suncalc-plot.txt', #outputfile. *.txt or *.html
            '--exportform': 'txt', # or 'html'
            '--plotwidth': 40,
            }
        return opt

    def loadcfg(self, filename):
        # ~ with open(filename) as json_file:
            # ~ return json.load(json_file)
        cfg = {
            'nl': '\n', #default for txt export
            'tab': '\t',        
        }
        
        if self.opt['--exportform'] == 'html':
            cfg['nl'] = '<br>'
        
        return cfg

        
    def parseArg(self, larg): #<list >opt-dict, word-list
        '''
        0 and 1 are handled as INT
        TODO isdigit für alle! !!! float geht nicht für isdigit!!!!????
        '''
        larg = larg[1:] #remove 1. arg (=caller name)
        for a in larg:
            if a.startswith("--"):
                l1 = a.split("=")
                self.opt[l1[0]] = l1[1]
                if l1[1] == "0":
                    self.opt[l1[0]] = 0
                elif l1[1] == "1":
                    self.opt[l1[0]] = 1
                        
            else:
                cupw("TODO: what to do with opt=", a)
                
        #args verarbeiten TODO: sollte besser gehen!
        self.opt['--datefrom'] = str(self.opt['--datefrom'])
        self.opt['--datetill'] = str(self.opt['--datetill'])
        self.opt['--lat'] = float(self.opt['--lat'])
        self.opt['--lon'] = float(self.opt['--lon'])
        self.opt['--elev'] = float(self.opt['--elev'])
        self.opt['--stepd'] = int(self.opt['--stepd'])
        self.opt['--plotwidth'] = int(self.opt['--plotwidth'])
    

###################################################3


    def getDDMMYYYY(self,date1): #<dt >dd.mm.yyyy(str)
        return date1.strftime("%d.%m.%Y")
        
    def getHHMM(self,date1): #<dt > HH:MM(str)
        return date1.strftime("%H:%M")
    
    def getDtFromHHMM(self,date1): #<dt >dt
        h = int(date1.strftime("%H"))
        m = int(date1.strftime("%M"))
        return datetime(2000,1,1,h,m)
    
    def getMinFromDtHHMM(self, date1): #<dt >total minutes(int)
        return int(date1.strftime("%H"))*60 + int(date1.strftime("%M"))
        
    def getDtFromYYYYMMDD(self, str1): #<YYYYMMDD(str) > dt
        return datetime.strptime(str1, '%Y%m%d')
    

###########################################
    def main(self): 
        
        self.outputText = ""
        self.outputText += self.opt['--place'] + " lat=" + str(self.opt['--lat']) + " lon=" + str(self.opt['--lon']) + self.cfg['nl']

        
        if self.opt['--prnminmax']:
            self.outputText += self.calcMinMax()
        
        if self.opt['--prnplot']:
            self.calcPlot()
            self.outputText += self.prnPlot()

        cupw("self.outputText=")
        print(self.outputText)
        with open(self.opt['--exportfile'], "w") as text_file:
            text_file.write(self.outputText)
            
        cupw("exported to", self.opt['--exportfile'])


        
        
        
    def calcPlot(self):
        
        self.opt['--dtfrom'] = self.getDtFromYYYYMMDD(self.opt['--datefrom'])
        self.opt['--dttill'] = self.getDtFromYYYYMMDD(self.opt['--datetill'])
        loc = Location((self.opt['--place'], self.opt['--region'], self.opt['--lat'], self.opt['--lon'], self.opt['--tz'], self.opt['--elev']))
        d = self.opt['--dtfrom']
        self.results = []
               
        while d <= self.opt['--dttill']:
            self.results.append(loc.sun(d, local=True))
            d = d + timedelta(days=self.opt['--stepd'])
        
        # ~ cupw("self.results", self.results)

        seq = [r['sunrise'].time() for r in self.results]
        minsunrise = min(seq)
        seq = [r['sunset'].time() for r in self.results]
        maxsunset = max(seq)
        # ~ cupw("minsunrise=", minsunrise, "maxsunset=", maxsunset )

        
        mint = self.getDtFromHHMM(minsunrise) #dt object min datum
        self.minm = self.getMinFromDtHHMM(mint) # minutes of min datum
        maxt = self.getDtFromHHMM(maxsunset) #dt object max datum
        maxday = maxt - mint
        # ~ cupw("maxday", maxday)
        self.diffm = maxday.total_seconds()/60  #max = 100%
        #cupw("self.minm", self.minm, "self.diffm=", self.diffm)
        # ~ cupw("diffm", self.diffm)
        
     

    def calcMinMax(self):
        '''
        fertigä seich das rechnen mit zeiten!!!
       
        '''
        dtnow = datetime.now()
        dtfrom = datetime(dtnow.year, 1, 1)
        dttill = datetime(dtnow.year, 12, 31)
        
        loc = Location((self.opt['--place'], self.opt['--region'], self.opt['--lat'], self.opt['--lon'], self.opt['--tz'], self.opt['--elev']))
        d = dtfrom
        self.results = []
        
        sunriseMin = datetime(dtnow.year, 1, 1, 23, 59)
        sunriseMax = datetime(dtnow.year, 1, 1, 0, 0)
        sunsetMin = datetime(dtnow.year, 1, 1, 23, 59)
        sunsetMax = datetime(dtnow.year, 1, 1, 0, 0)
        dayShortest = {'day': [], 'diffsec': 24*60*60}
        dayLongest = {'day': [], 'diffsec': 0}
               
        while d <= dttill:
            r = loc.sun(d, local=True)
            
            if r['sunrise'].time() < sunriseMin.time(): sunriseMin = r['sunrise']
            if r['sunrise'].time() > sunriseMax.time(): sunriseMax = r['sunrise']
            if r['sunset'].time() < sunsetMin.time(): sunsetMin = r['sunset']
            if r['sunset'].time() > sunsetMax.time(): sunsetMax = r['sunset']
            
            #shortest
            t1 = timedelta(hours=r['sunset'].hour, minutes=r['sunset'].minute, seconds=r['sunset'].second)
            t2 = timedelta(hours=r['sunrise'].hour, minutes=r['sunrise'].minute, seconds=r['sunrise'].second)
            delta = t1 - t2
            deltasec = delta.total_seconds()
            # ~ cupw("delta=", delta, "deltasec=", deltasec) #10:36:02
            if deltasec < dayShortest['diffsec']:
                dayShortest['day'] = r
                dayShortest['diffsec'] = deltasec
                
            #longest
            t1 = timedelta(hours=r['sunset'].hour, minutes=r['sunset'].minute, seconds=r['sunset'].second)
            t2 = timedelta(hours=r['sunrise'].hour, minutes=r['sunrise'].minute, seconds=r['sunrise'].second)
            delta = t1 - t2
            deltasec = delta.total_seconds()
            # ~ cupw("delta=", delta, "deltasec=", deltasec) #10:36:02
            if deltasec > dayLongest['diffsec']:
                dayLongest['day'] = r
                dayLongest['diffsec'] = deltasec
            
            # ~ cupw("r=", r, sunriseMin)
            d = d + timedelta(days=1)
 
        str1 = ""
        str1 += "sunrise earliest: " + str(sunriseMin) + self.cfg['nl']
        str1 += "sunset latest:    " + str(sunsetMax) + self.cfg['nl']
        str1 += "day longest:      " + dayLongest['day']['sunset'].strftime("%d.%m.%Y") + \
            " " + str(timedelta(seconds=dayLongest['diffsec'])) + self.cfg['nl']
        str1 += "sunrise latest:   " + str(sunriseMax) + self.cfg['nl']
        str1 += "sunset earliest:  " + str(sunsetMin) + self.cfg['nl']
        str1 += "day shortest:     " + dayShortest['day']['sunset'].strftime("%d.%m.%Y") + \
            " " + str(timedelta(seconds=dayShortest['diffsec'])) + self.cfg['nl']
        
        # ~ str1 += self.cfg['nl'] #abstand
        return str1


        
    def prnPlot(self):
        str1 = ""
        str1 += self.opt['--datefrom'] + " - " + self.opt['--datetill'] + self.cfg['nl']
        str1 += "date       daylong  *sunrise  -  no:on  -  sunset*" + self.cfg['nl']
        # ~ str1 += "          1          2        3         4         5         6         7" + self.cfg['nl']
        # ~ str1 += "0123456789*123456789*123456789*123456789*123456789*123456789*123456789*" + self.cfg['nl']
        # ~ str1 += "                             1          2        3         4         5" + self.cfg['nl']
        # ~ str1 += "                    123456789*123456789*123456789*123456789*123456789" + self.cfg['nl']
        # ~ str1 += "                    " + "." * self.opt['--plotwidth']  + self.cfg['nl']
        
        
        for r in self.results:
            
            str1 += self.getDDMMYYYY(r['sunrise']) + " "
            str1 += str(r['sunset'] - r['sunrise']) + " "
            
            strr = "." * self.opt['--plotwidth']
            
            #for testing see in string-replace1.py
            #sunrise
            m = self.getMinFromDtHHMM(r['sunrise']) - self.minm
            proz = int(self.opt['--plotwidth'] / self.diffm * m)
            col = proz
            str2 = "*" + self.getHHMM(r['sunrise'])
            #cupw("r['sunrise']=", r['sunrise'], "m=", m, "proz=", proz, "col", col)
            strr = strr[:col] + str2 + strr[(col+len(str2)):]
                        
            #noon
            m = self.getMinFromDtHHMM(r['noon']) - self.minm # datum minus the minimum
            col = int(self.opt['--plotwidth'] / self.diffm * m)
            str2 = self.getHHMM(r['noon'])
            lenstr2 = len(str2)
            lstr2 = int(lenstr2/2+0.5)
            rstr2 = lenstr2-lstr2
            strr = strr[:(col-lstr2)] + str2 + strr[(col+rstr2):]

            # ~ #sunset
            m = self.getMinFromDtHHMM(r['sunset']) - self.minm
            str2 = self.getHHMM(r['sunset']) + "*"
            lstr2 = len(str2)
            proz = int(self.opt['--plotwidth'] / self.diffm * m)
            col = proz - lstr2
            # cupw("plotwidth=']",self.opt['--plotwidth'], "proz=", proz, "col", col)
            strr = strr[:col] + str2 + strr[col+lstr2:]

            # cupw('strr', strr)
            str1 += strr
            str1 += self.cfg['nl']
            
        str1 += self.cfg['nl'] #abstand
        
        return str1
            
            
        
        
        
        
    
########################################
if __name__ == "__main__":
    
    larg = sys.argv #['arg1.py', '--test']
    app = Suncalcplot(larg) #works with: def __init__(self, *opt, **karg):
    cupw("app.opt=", app.opt)
    cupw("app.cfg=", app.cfg)
    
    app.main()
    
    
    sys.exit(app.errcode)
